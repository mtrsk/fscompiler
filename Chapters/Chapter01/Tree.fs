namespace Tree

module BinaryTree =
    type Tree<'T> =
        | Leaf
        | Node of Tree<'T> * 'T * Tree<'T>

    type TreeErrors =
        | LookupKeyError
        | LookupKeySuccess

    let rec insert(key, t) =
        match t with
        | Leaf -> Node(Leaf, key, Leaf)
        | Node(left, root, right) -> 
            if key < root then
                Node(insert(key, left), root, right)
            else if key = root then
                Node(left, key, right)
            else
                Node(left, root, insert(key, right))

    let rec isMember(key, t) =
        match t with
        | Leaf -> false
        | Node(left, h, right) ->
            if key = h then
                true
            else
                isMember(key, left) || isMember(key, right)

    let rec lookup(key, t) =
        match t with
        | Leaf -> None
        | Node(left, value, right) ->
            if key = value then
                Some value
            else if key < value then
                lookup(key, left)
            else
                lookup(key, right)


