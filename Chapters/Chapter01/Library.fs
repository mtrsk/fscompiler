namespace Library

module StraightLine =
    type Id = string
    
    type BinOp = Plus | Minus | Times | Div

    type Stm = 
        | CompoundStm of Stm*Stm
        | AssignStm of Id * Exp
        | PrintStm of Exp list
    and Exp =
        | IdExp of Id
        | NumExp of int
        | OpExp of Exp * BinOp * Exp
        | EseqExp of Stm * Exp
