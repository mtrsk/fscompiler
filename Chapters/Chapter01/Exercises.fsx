#load "Tree.fs"
open Tree.BinaryTree

// a) & b)

let t1 =
    Node(
        Node(Leaf, "b", Leaf),
        "a",
        Node(Leaf, "c", Leaf)
    )

let t2 =
    Node(
        Node(
            Node(Leaf, 20, Leaf),
            40,
            Node(Leaf, 60, Leaf)
        ),
        100,
        Node(
            Node(Leaf, 120, Leaf),
            150,
            Node(Leaf, 180, Leaf)
        )
    )

let a = insert(110, t2)

printfn "%b" (isMember(110, a))
