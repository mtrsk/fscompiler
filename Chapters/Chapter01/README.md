# Chapter 1: Introduction

## Programming Exercises

The programming exercises are coded in `Programming.fsx` and can be run via:

```
fsharpi --use:Programming.fsx
```
`prog` definition:
```
a := 5 + 3; b := (print(a, a-1), 10*a); print(b)
```

to test the `maxargs` function:

```
> maxargs prog;;
val it : int = 2
```

to test the interpreter:

```
> interp prog;;
8
7


80


val it : unit = ()
```

## Exercises

Regular exercises are also implemented in another script file, `Exercises.fsx`. To run:

```
fsharpi --use:Exercises.fsx
```