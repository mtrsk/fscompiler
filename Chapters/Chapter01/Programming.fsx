#load "Library.fs"
open Library.StraightLine

let prog =
    CompoundStm(
        AssignStm("a", OpExp(NumExp 5, Plus, NumExp 3)),
        CompoundStm(
            AssignStm("b",
                EseqExp(
                    PrintStm[IdExp "a"; OpExp(IdExp "a", Minus, NumExp 1)],
                    OpExp(NumExp 10, Times, IdExp "a")
                )
            ),
            PrintStm[IdExp "b"]
        )
    )

(*
    Write an ML (F#) function  (maxargs : smt -> int) that tells the maximum
    number of arguments of any print statement within any subexpression of a
    given statement. For example, maxargs prog is 2.
*)
let assignStmMatcher(expr : Exp) : Stm option =
    match expr with
    | EseqExp(s, _) -> Some s
    | _ -> None

let rec maxargs(stm : Stm) : int =
    match stm with
    | CompoundStm(left, right) -> max (maxargs(left)) (maxargs(right))
    | AssignStm(_, x) ->
        match assignStmMatcher x with
        | Some s -> maxargs s
        | None -> 0
    | PrintStm(x) -> List.length x

(*
    Write an ML (F#) function  (interp : smt -> unit) that "interprets" a
    program in this language.
*)

type Cell = Id * int
type Table = Cell list

let rec lookup(t : Table, id : Id) : int option =
    match t with
    | [] -> None
    | (x, value)::xs ->
        if x = id then
            Some value
        else
            lookup(xs, id)

let update(t : Table, id : Id, value : int) : Table =
    (id, value) :: t

let compute(binop : BinOp, x : int, y : int) =
    match binop with
    | Plus -> x + y
    | Minus -> x - y
    | Times -> x * y
    | Div -> x / y

let rec interpStm(stm : Stm, t : Table) : Table =
    match stm with
    | CompoundStm(left, right) ->
        interpStm(right, interpStm(left, t))
    | AssignStm(id, exp) ->
        let (value, t') = interpExp(exp, t)
        update(t', id, value)
    | PrintStm(expL) ->
        match expL with
        | [] ->
            printfn "\n"
            t
        | x::xs ->
            let (n, t') = interpExp(x, t)
            printfn "%i" n
            interpStm(PrintStm(xs), t')
and interpExp(exp : Exp, t : Table) : int * Table =
    match exp with
    | IdExp id ->
        match lookup(t, id) with
        | None -> failwith "No value for id"
        | Some v -> (v, t)
    | NumExp n -> (n, t)
    | OpExp(left, op, right) ->
        let (n, t') = interpExp(left, t)
        let (m, t'') = interpExp(right, t')
        (compute(op, n, m), t'')
    | EseqExp(stm, exp') ->
        let t' = interpStm(stm, t)
        interpExp(exp', t')

let interp(stm : Stm) : unit =
    interpStm(stm, []) |> ignore