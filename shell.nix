{ nixpkgs ? import <nixpkgs> {}
}:

with nixpkgs;

stdenv.mkDerivation {

  name = "dev-env";

  buildInputs = [
    dotnet-sdk
    git
  ];
}
